<?php    
    session_start();

    // https://github.com/akaAgar/vocabulaire-francais
    $dictionnary = ['allemand', 'argousier', 'belouga', 'canot', 'carrosse', 'divan', 'gravillon', 'luminaire', 'miroir', 'nuage', 'olivier'];
    
    // init session variables
    if (!isset($_SESSION['targetWord'])) {
      $_SESSION['targetWord'] = strtoupper($dictionnary[array_rand($dictionnary)]);
    }
    
    $targetWord = $_SESSION['targetWord'];
    
    if (!isset($_SESSION['nbTries'])) {
      $_SESSION['nbTries'] = 0;
    }
    
    if (!isset($_SESSION['sugg'])) {
      $_SESSION['sugg'] = array();
    }

    if (!isset($_SESSION['parties'])) {
      $_SESSION['parties'] = array();
    }

    $maxTries = 4;
    
    // Swiss keyboard
    $keyboard = [
      ['Q','W','E','R','T','Z','U','I','O','P'],
      ['A','S','D','F','G','H','J','K','L'],
      ['ENTER','Y','X','C','V','B','N','M', 'DEL'],
    ];
    
    
    // catch request, check if win or loose and create message
    if (isset($_REQUEST['suggestion']) && $_REQUEST['suggestion']) {
      $suggestion = $_REQUEST['suggestion'];

      if ($suggestion == $targetWord) {
        array_push($_SESSION['parties'], '<h2 style="color:green;">Mot: ' . $targetWord . ', Tentatives: ' . $_SESSION['nbTries']+1 . ', Victoire!</h2>');

        echo '<h1 style="color:green;">GAGNÉ</h1>';

        $suggestion = '';
        $_SESSION['nbTries'] = 0;
        $_SESSION['sugg'] = array();
        $_SESSION['targetWord'] = strtoupper($dictionnary[array_rand($dictionnary)]);

      } elseif ($_SESSION['nbTries'] >= $maxTries && in_array(strtolower($suggestion), $dictionnary)) {
        array_push($_SESSION['parties'], '<h2 style="color:red;">Mot: ' . $targetWord . ', Tentatives: ' . $_SESSION['nbTries']+1 . ', Défaite!</h2>');

        echo '<h1 style="color:red;">PERDU</h1>';

        $suggestion = '';
        $_SESSION['nbTries'] = 0;
        $_SESSION['sugg'] = array();
        $_SESSION['targetWord'] = strtoupper($dictionnary[array_rand($dictionnary)]);
      }
    }
    
    $targetWord = $_SESSION['targetWord'];

    // change words as array of letters
    $suggestionTab = str_split($suggestion);
    $targetWordTab = str_split($targetWord);
    ?>
<!doctype html>
<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <title>Wordle</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body{
            font-family: Arial, sans-serif;
        }
        h1{
            text-align: center;
        }
        .try{
            display: flex;
            justify-content: center;
            margin: auto;
        }
        .grid .letter{
            width: 50px;
            height: 50px;
            margin: 3px;
            font-size: 30px;
            font-weight: bold;
            border: 2px solid #ddd;
            text-align: center;
            line-height: 52px;
        }
        .grid .letter.good{
            background: green;
            border-color: green;
            color: white;
        }
        .grid .letter.position{
            background: #d5b427;
            border-color: #d5b427;
            color: white;
        }
        .keyboard{ margin-top: 80px; }
        .keyboard .row{
            display: flex;
            flex-direction: row;
            justify-content: center;
        }
        .keyboard .key{
            width: 43px;
            height: 58px;
            line-height: 60px;
            margin: 4px 3px;
            background: #ddd;
            font-weight: bold;
            text-align: center;
            border-radius: 3px; 
            cursor: pointer;
            user-select: none;
        }
        .keyboard .key.function{
            width: auto;
            padding: 0 10px;
        }
    </style>
</head>
<body>
  <h1>Wordle</h1>
  <div class="grid">
    <?php

      $intermediatetargetWordTab = $targetWordTab;
      
      // check if input word is in dictionnary
      if (in_array(strtolower($suggestion), $dictionnary)) {

        $letterDisplay = array();
        
        // for each letter in input word check if it present in the target word and if its at the same index
        foreach ($suggestionTab as $sug) {
          if (in_array($sug, $intermediatetargetWordTab)) {
            
            $indexTarget = array_search($sug, $targetWordTab);
            $indexSug = array_search($sug, $suggestionTab);
            
            $classe = 'letter position';
            
            if ($indexSug == $indexTarget) {
              $classe = 'letter good';
            }

            // avoid 'letter position' if there are duplicate letters in the target word
            $intermediatetargetWordTab[$indexTarget] = '';
            
          } else {
            $classe = 'letter';
          }
          
          array_push($letterDisplay, '<div class="' . $classe . '">' . $sug . '</div>');
          
        }
        
        array_push($_SESSION['sugg'], $letterDisplay);      
        $_SESSION['nbTries']++; 
        
      }
      
      ?>
      <!-- Affichage -->
      <?php
        for ($i = 0; $i < $_SESSION['nbTries']; $i++) :
          ?>
      <div class="try">
        <?php 
          for ($j = 0; $j < count($_SESSION['sugg'][$i]); $j++) {
            echo $_SESSION['sugg'][$i][$j];
          }
        ?>
      </div>
      <?php endfor ?>
      
      <div class="try editable">
        <?php
          for ($i = 0; $i < strlen($targetWord); $i++) {
            echo '<div class="letter"></div>';
          }
        ?>
      </div>
    </div>
    
    <form method="post" action="wordle.php">
        <input type="hidden" name="suggestion" />
    </form>
    
    <div class="keyboard">
      <?php
        foreach ($keyboard as $row) {
          echo '<div class="row">';
          foreach ($row as $key) {
            $classes = [];
            
            if (in_array($key, ['ENTER', 'DEL'])) {
              $classes[] = 'function';
            }
            
            echo '<div class="key ' . implode(' ', $classes) . '" data-key="' . strtolower($key) . '">' . $key . '</div>';
          }
          echo '</div>';
        }
        ?>
    </div>
    <div style="width: 100%; margin-top: 5%; text-align:center;">
      <?php
        foreach ($_SESSION['parties'] as $party) {
          echo $party;
        }
      ?>
  </div>
    <script>
        const keys = document.querySelectorAll('.keyboard .key:not(.function)'),
              functionKeys = document.querySelectorAll('.keyboard .key.function'),
              input = document.querySelector('input[name="suggestion"]'),
              wordle = document.querySelectorAll('.try.editable .letter'),
              form = document.querySelector('form')

        let position = 0

        // Reset field
        input.value = ''    
        
        // Real keyboard
        document.addEventListener('keydown', (event) => {
            // Letter
            if (event.keyCode >= 65 && event.keyCode <= 90) {
                addLetter(event.key.toUpperCase())
            
            // Enter
            } else if (event.keyCode == 13) {
                // Suggestion is fullfilled
                if (position == wordle.length) {
                    form.submit()
                }

            // Delete
            } else if (event.keyCode == 8) {
                event.stopPropagation()
                removeLetter()
            }
        })
        
        // Regular keyboard keys
        keys.forEach((key) => {
            key.addEventListener('click', (event) => {
                addLetter(event.target.innerText)                
            }, false)
        })
        
        // Function keys
        functionKeys.forEach((key) => {
            key.addEventListener('click', (event) => {
                let action = event.target.innerText 
                
                switch (action) {
                    // Submit word
                    case 'ENTER':
                        // Suggestion is fullfilled
                        if (position == wordle.length) {
                            form.submit()
                        }
                        break;
                        
                    // Delete last letter
                    case 'DEL':
                        removeLetter()
                        break;
                }
            }, false)
        })
        
        // Add letter to suggestion
        function addLetter(letter) {
            if (position < wordle.length) {
                input.value += letter
                wordle[position].innerText = letter
                
                position++;
            }
        }
        
        // Remove last letter from suggestion
        function removeLetter() {
            if (position > 0) {
                position--
                wordle[position].innerText = ''
                input.value = input.value.slice(0, -1)
            }
        }
    </script>
</body>